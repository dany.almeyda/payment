package com.bbva.pzic.payments.facade.v0.mapper.impl;

import com.bbva.pzic.payments.EntityMock;
import com.bbva.pzic.payments.business.dto.DTOIntAutomaticBillPayment;
import com.bbva.pzic.payments.canonic.AutomaticBillPayment;
import com.bbva.pzic.payments.facade.v0.mapper.ICreateAutomaticBillPaymentsMapper;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class CreateAutomaticBillPaymentsMapperTest {

    private ICreateAutomaticBillPaymentsMapper mapper;

    @Before
    public void setUp() {
        mapper = new CreateAutomaticBillPaymentsMapper();
    }
    @Test
    public void mapInTest() throws IOException {
        AutomaticBillPayment entity = EntityMock.getInstance().getAutomaticBillPayment();
        DTOIntAutomaticBillPayment result = mapper.mapIn(entity);

        assertNull(result.getId());
        assertNotNull(result.getService().getId());
        assertNotNull(result.getService().getServiceType().getId());
        assertNotNull(result.getBill().getId());
        assertNotNull(result.getStartDate());
        assertNotNull(result.getOrigin().getContractId());
        assertNotNull(result.getAmountMax().getValue());
        assertNotNull(result.getAmountMax().getCurrency());
        assertNotNull(result.getAdditionalFields().getValue());

        assertEquals(entity.getService().getId(), result.getService().getId());
        assertEquals(entity.getService().getServiceType().getId(), result.getService().getServiceType().getId());
        assertEquals(entity.getBill().getId(), result.getBill().getId());
        assertEquals(entity.getStartDate(), result.getStartDate());
        assertEquals(entity.getOrigin().getContractId(), result.getOrigin().getContractId());
        assertEquals(entity.getAmountMax().getValue(), result.getAmountMax().getValue());
        assertEquals(entity.getAmountMax().getCurrency(), result.getAmountMax().getCurrency());
        assertEquals(entity.getAdditionalFields().get(0).getValue(), result.getAdditionalFields().getValue());
    }

}
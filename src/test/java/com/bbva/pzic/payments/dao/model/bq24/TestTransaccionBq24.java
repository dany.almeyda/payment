package com.bbva.pzic.payments.dao.model.bq24;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.junit.Test;
import org.junit.Ignore;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;


import com.bbva.jee.arq.spring.core.host.protocolo.ExcepcionRespuestaHost;
import com.bbva.jee.arq.spring.core.host.transporte.ExcepcionTransporte;


/**
 * Test de la transacci&oacute;n <code>BQ24</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Ignore
@RunWith(MockitoJUnitRunner.class)
public class TestTransaccionBq24 {

	@InjectMocks
	private TransaccionBq24 transaccion;

	@Mock
	private ServicioTransacciones servicioTransacciones;

	@Test
	public void test() throws ExcepcionTransaccion {

		PeticionTransaccionBq24 peticion = new PeticionTransaccionBq24();

		RespuestaTransaccionBq24 respuesta = transaccion.invocar(peticion);

		Mockito.when(servicioTransacciones.invocar(PeticionTransaccionBq24.class, RespuestaTransaccionBq24.class,
				peticion)).thenReturn(respuesta);

		RespuestaTransaccionBq24 result = transaccion.invocar(peticion);

		Assert.assertEquals(result, respuesta);
	}
}
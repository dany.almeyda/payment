package com.bbva.pzic.payments.dao.tx.mapper.impl;

import com.bbva.pzic.payments.dao.tx.mapper.ITxListAutomaticBillPaymentsMapper;
import org.junit.Before;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
public class TxListAutomaticBillPaymentsMapperTest {

    private ITxListAutomaticBillPaymentsMapper mapper;

    @Before
    public void setUp() {
        mapper = new TxListAutomaticBillPaymentsMapper();
    }
}
package com.bbva.pzic.payments;

import com.bbva.pzic.payments.business.dto.DTOIntAutomaticBillPayments;
import com.bbva.pzic.payments.canonic.AutomaticBillPayment;
import com.bbva.pzic.payments.util.mappers.ObjectMapperHelper;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * @author Entelgy
 */
public final class EntityMock {

    public final static String PAGINATION_KEY = "G";
    public final static Integer PAGE_SIZE = 4;
    public final static String STATUS_ID = "S";
    public final static String SERVICE_SERVICE_TYPE_ID = "3";
    public final static String SERVICE_ID = "o";
    public final static String FROM_START_DATE = "g";
    public final static String TO_START_DATE = "X";
    private static final EntityMock INSTANCE = new EntityMock();
    private ObjectMapperHelper objectMapper = ObjectMapperHelper.getInstance();

    private EntityMock() {

    }

    public static EntityMock getInstance() {
        return INSTANCE;
    }

    public DTOIntAutomaticBillPayments getDTOIntAutomaticBillPayments() {
        DTOIntAutomaticBillPayments dtoIntAutomaticBillPayments = new DTOIntAutomaticBillPayments();
        dtoIntAutomaticBillPayments
                .setData(new ArrayList<AutomaticBillPayment>());
        return dtoIntAutomaticBillPayments;
    }

    public AutomaticBillPayment getAutomaticBillPayment() throws IOException {
        return objectMapper.readValue(Thread.currentThread().getContextClassLoader()
                        .getResourceAsStream("mock/automaticBillPayment.json"), AutomaticBillPayment.class);
    }

}
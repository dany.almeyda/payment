package com.bbva.pzic.payments.facade.v0.mapper;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.payments.business.dto.DTOIntAutomaticBillPayment;
import com.bbva.pzic.payments.canonic.AutomaticBillPayment;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
public interface ICreateAutomaticBillPaymentsMapper {

    DTOIntAutomaticBillPayment mapIn(AutomaticBillPayment automaticBillPayment);

    ServiceResponse<AutomaticBillPayment> mapOut(AutomaticBillPayment automaticBillPayment);
}
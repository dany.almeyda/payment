package com.bbva.pzic.payments.facade.v0;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.payments.business.dto.DTOIntAutomaticBillPayment;
import com.bbva.pzic.payments.canonic.AutomaticBillPayment;
import com.bbva.pzic.payments.canonic.AutomaticBillPaymentData;
import com.bbva.pzic.payments.canonic.AutomaticBillPayments;

import javax.ws.rs.core.Response;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
public interface ISrvPaymentsV0 {

    /**
     * Method for retrieving the list of automatic bill payments configured by
     * the customer.
     *
     * @param paginationKey        key to obtain a single page
     * @param pageSize             number of elements per page
     * @param statusId             filters the status of automatic bill payments.
     * @param serviceServiceTypeId filters the list of automatic bill payments by service type.
     * @param serviceId            filters the list of automatic bill payments by service.
     * @param fromStartDate        filters the list of automatic bill payments whose date is
     *                             newer than the provided by this parameter (ISO-8601 date
     *                             format).
     * @param toStartDate          filters the list of automatic bill payments whose date is
     *                             older than the provided by this parameter (ISO-8601 date
     *                             format).
     * @return {@link AutomaticBillPayments}
     */
    AutomaticBillPayments listAutomaticBillPayments(String paginationKey, Integer pageSize, String statusId, String serviceServiceTypeId, String serviceId,
                                                    String fromStartDate, String toStartDate);

    /**
     * Method for performing the automatic bill payment of a service.
     *
     * @param automaticBillPayment payload
     * @return {@link AutomaticBillPaymentData}
     */

    ServiceResponse<AutomaticBillPayment> createAutomaticBillPayments(AutomaticBillPayment automaticBillPayment);
}
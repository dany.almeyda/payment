package com.bbva.pzic.payments.facade.v0.mapper.impl;

import com.bbva.jee.arq.spring.core.catalog.gabi.ServiceResponse;
import com.bbva.pzic.payments.business.dto.*;
import com.bbva.pzic.payments.canonic.*;
import com.bbva.pzic.payments.facade.v0.mapper.ICreateAutomaticBillPaymentsMapper;
import com.bbva.pzic.payments.util.mappers.Mapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.List;

@Mapper("createAutomaticBillPaymentsMapper")
public class CreateAutomaticBillPaymentsMapper implements ICreateAutomaticBillPaymentsMapper {

    private static final Log LOG = LogFactory
            .getLog(CreateAutomaticBillPaymentsMapper.class);

    @Override
    public DTOIntAutomaticBillPayment mapIn(
            final AutomaticBillPayment automaticBillPayment) {
        LOG.info("... called method CreateAutomaticBillPaymentsMapper.mapIn ...");
        DTOIntAutomaticBillPayment dtoInt = new DTOIntAutomaticBillPayment();
        dtoInt.setId(automaticBillPayment.getId());
        dtoInt.setService(mapInService(automaticBillPayment.getService()));
        dtoInt.setBill(mapInBill(automaticBillPayment.getBill()));
        dtoInt.setStartDate(automaticBillPayment.getStartDate());
        dtoInt.setOrigin(mapInOrigin(automaticBillPayment.getOrigin()));
        dtoInt.setAmountMax(mapInAmountMax(automaticBillPayment.getAmountMax()));
        dtoInt.setAdditionalFields(mapInSetAdditionalFields(automaticBillPayment.getAdditionalFields()));
        return dtoInt;
    }


    private DTOIntAdditionalField mapInSetAdditionalFields(List<AdditionalField> additionalFields) {
        if(additionalFields.get(0).getValue()==null){
            return null;
        }
        DTOIntAdditionalField dtoIntAdditionalFields = new DTOIntAdditionalField();
        dtoIntAdditionalFields.setValue(additionalFields.get(0).getValue());
        return dtoIntAdditionalFields;
    }

    private DTOIntAmountMax mapInAmountMax(AmountMax amountMax) {
        if(amountMax == null){
            return null;
        }
        DTOIntAmountMax dtoIntAmountMax = new DTOIntAmountMax();
        dtoIntAmountMax.setCurrency(amountMax.getCurrency());
        dtoIntAmountMax.setValue(amountMax.getValue());
        return dtoIntAmountMax;
    }

    private DTOIntOrigin mapInOrigin(Origin origin) {
        if(origin == null){
            return null;
        }
        DTOIntOrigin dtoIntOrigin = new DTOIntOrigin();
        dtoIntOrigin.setContractId(origin.getContractId());
        return dtoIntOrigin;
    }

    private DTOIntBill mapInBill(final Bill bill) {
        if(bill == null){
            return null;
        }
        DTOIntBill dtoIntBill = new DTOIntBill();
        dtoIntBill.setId(bill.getId());
        return dtoIntBill;
    }

    private DTOIntService mapInService(final Service service) {
        if(service == null){
            return null;
        }
        DTOIntService dtoIntService = new DTOIntService();
        dtoIntService.setId(service.getId());
        dtoIntService.setServiceType(mapInServiceType(service.getServiceType()));
        return dtoIntService;
    }

    private DTOIntServiceType mapInServiceType(final ServiceType serviceType) {
        if(serviceType == null){
            return null;
        }
        DTOIntServiceType dtoIntServiceType = new DTOIntServiceType();
        dtoIntServiceType.setId(serviceType.getId());
        return dtoIntServiceType;
    }

    @Override
    public ServiceResponse<AutomaticBillPayment> mapOut(
            final AutomaticBillPayment dto) {
        LOG.info("... called method CreateAutomaticBillPaymentsMapper.mapOut ...");
        if (dto == null) {
            return null;
        }
        return ServiceResponse.data(dto).build();
    }


}


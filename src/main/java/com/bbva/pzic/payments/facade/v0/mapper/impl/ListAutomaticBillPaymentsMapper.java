package com.bbva.pzic.payments.facade.v0.mapper.impl;

import com.bbva.pzic.payments.business.dto.DTOIntAutomaticBillPayments;
import com.bbva.pzic.payments.business.dto.InputListAutomaticBillPayments;
import com.bbva.pzic.payments.canonic.AutomaticBillPayments;
import com.bbva.pzic.payments.facade.v0.mapper.IListAutomaticBillPaymentsMapper;
import com.bbva.pzic.payments.util.mappers.Mapper;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
@Mapper
public class ListAutomaticBillPaymentsMapper
        implements
        IListAutomaticBillPaymentsMapper {

    private static final Log LOG = LogFactory
            .getLog(ListAutomaticBillPaymentsMapper.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public InputListAutomaticBillPayments mapIn(final String paginationKey,
                                                final Integer pageSize, final String statusId,
                                                final String serviceServiceTypeId, final String serviceId,
                                                final String fromStartDate, final String toStartDate) {
        LOG.info("... called method ListAutomaticBillPaymentsMapper.mapIn ...");
        InputListAutomaticBillPayments input = new InputListAutomaticBillPayments();
        input.setPaginationKey(paginationKey);
        input.setPageSize(pageSize);
        input.setStatusId(statusId);
        input.setServiceServiceTypeId(serviceServiceTypeId);
        input.setServiceId(serviceId);
        input.setFromStartDate(fromStartDate);
        input.setToStartDate(toStartDate);
        return input;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AutomaticBillPayments mapOut(
            final DTOIntAutomaticBillPayments dtoIntAutomaticBillPayments) {
        LOG.info("... called method ListAutomaticBillPaymentsMapper.mapOut ...");
        if (dtoIntAutomaticBillPayments == null) {
            return null;
        }
        AutomaticBillPayments automaticBillPayments = new AutomaticBillPayments();
        automaticBillPayments.setData(dtoIntAutomaticBillPayments.getData());
        return automaticBillPayments;
    }
}
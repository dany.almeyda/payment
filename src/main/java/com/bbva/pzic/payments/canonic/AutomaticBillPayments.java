package com.bbva.pzic.payments.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;
import java.util.List;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "automaticBillPayments", namespace = "urn:com:bbva:pzic:payments:canonic")
@XmlType(name = "automaticBillPayments", namespace = "urn:com:bbva:pzic:payments:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class AutomaticBillPayments implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * AutomaticBillPayment Data List
     */
    private List<AutomaticBillPayment> data;
    /**
     * Object related to the pagination
     */
    private Pagination pagination;

    public List<AutomaticBillPayment> getData() {
        return data;
    }

    public void setData(List<AutomaticBillPayment> data) {
        this.data = data;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }
}
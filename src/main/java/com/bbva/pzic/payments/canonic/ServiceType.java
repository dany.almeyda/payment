package com.bbva.pzic.payments.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "serviceType", namespace = "urn:com:bbva:pzic:payments:canonic")
@XmlType(name = "serviceType", namespace = "urn:com:bbva:pzic:payments:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class ServiceType implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * Identifier associated to the service type.
     */
    private String id;
    /**
     * Name of the service type.
     */
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
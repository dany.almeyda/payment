package com.bbva.pzic.payments.canonic;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import java.io.Serializable;

/**
 * @author Entelgy
 */
@XmlRootElement(name = "automaticBillPaymentData", namespace = "urn:com:bbva:pzic:payments:canonic")
@XmlType(name = "automaticBillPaymentData", namespace = "urn:com:bbva:pzic:payments:canonic")
@XmlAccessorType(XmlAccessType.FIELD)
public class AutomaticBillPaymentData implements Serializable {

    private static final long serialVersionUID = 1L;
    /**
     * AutomaticBillPayment Data
     */
    private AutomaticBillPayment data;

    public AutomaticBillPayment getData() {
        return data;
    }

    public void setData(AutomaticBillPayment data) {
        this.data = data;
    }
}
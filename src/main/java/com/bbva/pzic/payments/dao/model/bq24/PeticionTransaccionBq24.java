package com.bbva.pzic.payments.dao.model.bq24;

import com.bbva.jee.arq.spring.core.host.Atributo;
import com.bbva.jee.arq.spring.core.host.Cuerpo;
import com.bbva.jee.arq.spring.core.host.CuerpoMultiparte;
import com.bbva.jee.arq.spring.core.host.MensajeMultiparte;
import com.bbva.jee.arq.spring.core.host.Multiformato;
import com.bbva.jee.arq.spring.core.host.Transaccion;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;


/**
 * <p>Transacci&oacute;n <code>BQ24</code></p>
 * <p>Informaci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Tipo:</b> 1</li>
 *    <li><b>Subtipo:</b> 1</li>
 *    <li><b>Versi&oacute;n:</b> 1</li>
 *    <li><b>Clase de petici&oacute;n:</b> PeticionTransaccionBq24</li>
 *    <li><b>Clase de respuesta:</b> RespuestaTransaccionBq24</li>
 * </ul>
 * </p>
 * <p>Configuraci&oacute;n de la transacci&oacute;n:
 * <ul>
 *    <li><b>Nombre configuraci&oacute;n:</b> default_ps9_mx</li>
 * </ul>
 * </p>
 * <p>Copy de la transacci&oacute;n:</p>
 * <code><pre> * FICHERO: PEBT.QGFD.FIX.QGDTCCT.BQ24.D1170920
 * BQ24PAGO FACIL-ALTA DE SERVICIO        BG        BQ2C0240BVDBGPO BGMQ24E             BQ24  NS0000CNNNNN    SSTN    C   SNNNSNNN  NN                2016-11-15XP87051 2017-03-1711.31.56XP86453 2016-11-15-08.54.01.538248XP87051 0001-01-010001-01-01
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.BGMQ24E.D1170920
 * BGMQ24E �PAGO FACIL-ALTA DE SERVICIO   �F�09�00108�01�00001�NUMCLIE�CODIGO DE CLIENTE   �A�008�0�O�        �
 * BGMQ24E �PAGO FACIL-ALTA DE SERVICIO   �F�09�00108�02�00009�CONVENI�CODIGO DE CONVENIO  �A�009�0�R�        �
 * BGMQ24E �PAGO FACIL-ALTA DE SERVICIO   �F�09�00108�03�00018�CODSUMI�CODIGO DE SUMINISTRO�A�030�0�R�        �
 * BGMQ24E �PAGO FACIL-ALTA DE SERVICIO   �F�09�00108�04�00048�FECPAG �FECHA PAGO DE RECIBO�A�010�0�O�        �
 * BGMQ24E �PAGO FACIL-ALTA DE SERVICIO   �F�09�00108�05�00058�IDENCON�NUMERO DE CONTRATO  �A�020�0�R�        �
 * BGMQ24E �PAGO FACIL-ALTA DE SERVICIO   �F�09�00108�06�00078�MAXPAGA�TOPE MAX. IMP. PAGAR�N�015�2�O�        �
 * BGMQ24E �PAGO FACIL-ALTA DE SERVICIO   �F�09�00108�07�00093�DIVPAGA�DIVISA DE IMP. PAGAR�A�003�0�R�        �
 * BGMQ24E �PAGO FACIL-ALTA DE SERVICIO   �F�09�00108�08�00096�PRIORID�PRIORIDAD ORDEN PAGO�N�003�0�O�        �
 * BGMQ24E �PAGO FACIL-ALTA DE SERVICIO   �F�09�00108�09�00099�FECRETE�FECHA DE RETENCION  �A�010�0�O�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDF.BGMQ24S.D1170920
 * BGMQ24S �PAGO FACIL-ALTA DE SERVICIO   �X�01�00047�01�00001�IDAFIL �IDENT. DE AFILIACION�A�047�0�S�        �
 * FICHERO: PEBT.QGFD.FIX.QGDTFDX.BQ24.D1170920
 * BQ24BGMQ24S BGNC24S BQ2C02401S                             XP86453 2016-12-19-13.45.18.128317XP86453 2016-12-19-13.45.18.128332
</pre></code>
 * 
 * @see RespuestaTransaccionBq24
 *
 * @author Arquitectura Spring BBVA
 */
@Transaccion(
	nombre = "BQ24",
	tipo = 1, 
	subtipo = 1,	
	version = 1,
	configuracion = "default_ps9_mx",
	respuesta = RespuestaTransaccionBq24.class,
	atributos = {@Atributo(nombre = "altamiraExtendido", valor = "true"), @Atributo(nombre = "tipoCopy", valor = "FIJA")}
)
@Multiformato(formatos = {FormatoBGMQ24E.class})
@RooJavaBean
@RooSerializable
public class PeticionTransaccionBq24 implements MensajeMultiparte {
	
	/**
	 * <p>Cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Cuerpo
	private CuerpoMultiparte cuerpo = new CuerpoMultiparte();
	
	/**
	 * <p>Permite obtener el cuerpo del mensaje de petici&oacute;n multiparte</p>
	 */
	@Override
	public CuerpoMultiparte getCuerpo() {
		return cuerpo;
	}
	
}
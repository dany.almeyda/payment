package com.bbva.pzic.payments.dao.tx.mapper.impl;

import com.bbva.pzic.payments.business.dto.DTOIntAdditionalField;
import com.bbva.pzic.payments.business.dto.DTOIntAutomaticBillPayment;
import com.bbva.pzic.payments.canonic.AutomaticBillPayment;
import com.bbva.pzic.payments.canonic.AutomaticBillPaymentData;
import com.bbva.pzic.payments.dao.model.bq24.FormatoBGMQ24E;
import com.bbva.pzic.payments.dao.model.bq24.FormatoBGMQ24S;
import com.bbva.pzic.payments.dao.tx.mapper.ITxCreateAutomaticBillPaymentsMapper;
import com.bbva.pzic.payments.util.mappers.Mapper;
import org.springframework.util.CollectionUtils;

import java.util.List;


@Mapper("txCreateAutomaticBillPaymentsMapper")
public class TxCreateAutomaticBillPaymentsMapper implements ITxCreateAutomaticBillPaymentsMapper {

    @Override
    public FormatoBGMQ24E mapIn(final DTOIntAutomaticBillPayment dtoIn) {
        FormatoBGMQ24E format = new FormatoBGMQ24E();
        format.setConveni(dtoIn.getService().getId().concat(dtoIn.getService().getServiceType().getId()));
        format.setCodsumi(dtoIn.getBill().getId());
        format.setFecpag(dtoIn.getStartDate());
        format.setIdencon(dtoIn.getOrigin().getContractId());
        format.setMaxpaga(dtoIn.getAmountMax().getValue());
        format.setDivpaga(dtoIn.getAmountMax().getCurrency());
        format.setPriorid(mapInPriorId(dtoIn.getAdditionalFields()));
        return format;
    }


    private Integer mapInPriorId(final DTOIntAdditionalField additionalFields) {
        if( additionalFields == null){
            return null;
        }
        return Integer.parseInt(additionalFields.getValue());
    }

    @Override
    public AutomaticBillPayment mapOut(final FormatoBGMQ24S formatOutput, DTOIntAutomaticBillPayment dtoOut) {
        if (formatOutput == null) {
            return null;
        }
        AutomaticBillPayment automaticBillPayment = new AutomaticBillPayment();
        automaticBillPayment.setId(formatOutput.getIdafil());
        return automaticBillPayment;
    }
}
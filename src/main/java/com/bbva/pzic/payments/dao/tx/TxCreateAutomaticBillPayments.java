package com.bbva.pzic.payments.dao.tx;

import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;
import com.bbva.pzic.payments.business.dto.DTOIntAutomaticBillPayment;
import com.bbva.pzic.payments.canonic.AutomaticBillPayment;
import com.bbva.pzic.payments.canonic.AutomaticBillPaymentData;
import com.bbva.pzic.payments.dao.model.bq24.FormatoBGMQ24E;
import com.bbva.pzic.payments.dao.model.bq24.FormatoBGMQ24S;
import com.bbva.pzic.payments.dao.model.bq24.PeticionTransaccionBq24;
import com.bbva.pzic.payments.dao.model.bq24.RespuestaTransaccionBq24;
import com.bbva.pzic.payments.dao.tx.mapper.ITxCreateAutomaticBillPaymentsMapper;
import com.bbva.pzic.routine.commons.utils.host.templates.Tx;
import com.bbva.pzic.routine.commons.utils.host.templates.impl.SingleOutputFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import javax.annotation.Resource;

@Tx("createAutomaticBillPayments")
public class TxCreateAutomaticBillPayments extends SingleOutputFormat<DTOIntAutomaticBillPayment, FormatoBGMQ24E, AutomaticBillPayment, FormatoBGMQ24S> {

    @Resource(name = "txCreateAutomaticBillPaymentsMapper")
    private ITxCreateAutomaticBillPaymentsMapper mapper;

    @Autowired
    public TxCreateAutomaticBillPayments(@Qualifier("transaccionBq24") InvocadorTransaccion<PeticionTransaccionBq24, RespuestaTransaccionBq24> transaction) {
        super(transaction, PeticionTransaccionBq24::new, AutomaticBillPayment::new, FormatoBGMQ24S.class);
    }

    @Override
    protected FormatoBGMQ24E mapInput(DTOIntAutomaticBillPayment dtoIntAutomaticBillPayment) {
        return mapper.mapIn(dtoIntAutomaticBillPayment);
    }

@Override
protected AutomaticBillPayment mapFirstOutputFormat(FormatoBGMQ24S formatoBGMQ24S, DTOIntAutomaticBillPayment dtoIntAutomaticBillPayment, AutomaticBillPayment automaticBillPayment) {
    return mapper.mapOut(formatoBGMQ24S, dtoIntAutomaticBillPayment);
}
}

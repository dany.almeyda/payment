package com.bbva.pzic.payments.dao.model.bq24;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.bbva.jee.arq.spring.core.host.ExcepcionTransaccion;
import com.bbva.jee.arq.spring.core.host.ServicioTransacciones;
import com.bbva.jee.arq.spring.core.host.InvocadorTransaccion;

/**
 * Invocador de la transacci&oacute;n <code>BQ24</code>
 * 
 * @see PeticionTransaccionBq24
 * @see RespuestaTransaccionBq24
 */
@Component
public class TransaccionBq24 implements InvocadorTransaccion<PeticionTransaccionBq24,RespuestaTransaccionBq24> {
	
	@Autowired
	private ServicioTransacciones servicioTransacciones;
	
	@Override
	public RespuestaTransaccionBq24 invocar(PeticionTransaccionBq24 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionBq24.class, RespuestaTransaccionBq24.class, transaccion);
	}
	
	@Override
	public RespuestaTransaccionBq24 invocarCache(PeticionTransaccionBq24 transaccion) {
		return servicioTransacciones.invocar(PeticionTransaccionBq24.class, RespuestaTransaccionBq24.class, transaccion);
	}
	
	@Override
	public void vaciarCache() {
		//this method does not have to be used anymore
	}
}
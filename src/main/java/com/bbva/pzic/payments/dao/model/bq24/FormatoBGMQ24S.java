package com.bbva.pzic.payments.dao.model.bq24;

import com.bbva.jee.arq.spring.core.host.Campo;
import com.bbva.jee.arq.spring.core.host.TipoCampo;
import com.bbva.jee.arq.spring.core.host.Formato;

import org.springframework.roo.addon.javabean.annotations.RooJavaBean;
import org.springframework.roo.addon.javabean.annotations.RooToString;
import org.springframework.roo.addon.javabean.annotations.RooSerializable;



/**
 * Formato de datos <code>BGMQ24S</code> de la transacci&oacute;n <code>BQ24</code>
 * 
 * @author Arquitectura Spring BBVA
 */
@Formato(nombre = "BGMQ24S")
@RooJavaBean
@RooSerializable
public class FormatoBGMQ24S {
	
	/**
	 * <p>Campo <code>IDAFIL</code>, &iacute;ndice: <code>1</code>, tipo: <code>ALFANUMERICO</code>
	 */
	@Campo(indice = 1, nombre = "IDAFIL", tipo = TipoCampo.ALFANUMERICO, longitudMinima = 47, longitudMaxima = 47)
	private String idafil;
	
}
package com.bbva.pzic.payments.dao.tx.mapper;

import com.bbva.pzic.payments.business.dto.DTOIntAutomaticBillPayment;
import com.bbva.pzic.payments.canonic.AutomaticBillPayment;
import com.bbva.pzic.payments.dao.model.bq24.FormatoBGMQ24E;
import com.bbva.pzic.payments.dao.model.bq24.FormatoBGMQ24S;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
public interface ITxCreateAutomaticBillPaymentsMapper {

    FormatoBGMQ24E mapIn(DTOIntAutomaticBillPayment dtoIn);

    AutomaticBillPayment mapOut(FormatoBGMQ24S formatOutput, DTOIntAutomaticBillPayment dtoOut);

}
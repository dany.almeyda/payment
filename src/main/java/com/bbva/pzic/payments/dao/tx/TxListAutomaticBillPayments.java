package com.bbva.pzic.payments.dao.tx;

import com.bbva.pzic.payments.dao.tx.mapper.ITxListAutomaticBillPaymentsMapper;
import com.bbva.pzic.payments.util.tx.Tx;

import javax.annotation.Resource;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
@Tx("txListAutomaticBillPayments")
public class TxListAutomaticBillPayments {

    @Resource(name = "txListAutomaticBillPaymentsMapper")
    private ITxListAutomaticBillPaymentsMapper mapper;
}
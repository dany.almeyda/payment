package com.bbva.pzic.payments.business.dto;

import javax.validation.constraints.NotNull;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
public class DTOIntOrigin {

    @NotNull(groups = ValidationGroup.CreateAutomaticBillPayments.class)
    private String contractId;

    public String getContractId() {
        return contractId;
    }

    public void setContractId(String contractId) {
        this.contractId = contractId;
    }
}
package com.bbva.pzic.payments.business.dto;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
public class InputListAutomaticBillPayments {

    private String paginationKey;
    private Integer pageSize;
    private String statusId;
    private String serviceServiceTypeId;
    private String serviceId;
    private String fromStartDate;
    private String toStartDate;

    public String getPaginationKey() {
        return paginationKey;
    }

    public void setPaginationKey(String paginationKey) {
        this.paginationKey = paginationKey;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public String getStatusId() {
        return statusId;
    }

    public void setStatusId(String statusId) {
        this.statusId = statusId;
    }

    public String getServiceServiceTypeId() {
        return serviceServiceTypeId;
    }

    public void setServiceServiceTypeId(String serviceServiceTypeId) {
        this.serviceServiceTypeId = serviceServiceTypeId;
    }

    public String getServiceId() {
        return serviceId;
    }

    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    public String getFromStartDate() {
        return fromStartDate;
    }

    public void setFromStartDate(String fromStartDate) {
        this.fromStartDate = fromStartDate;
    }

    public String getToStartDate() {
        return toStartDate;
    }

    public void setToStartDate(String toStartDate) {
        this.toStartDate = toStartDate;
    }
}
package com.bbva.pzic.payments.business.dto;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
public class DTOIntAmountMax {

    @NotNull(groups = ValidationGroup.CreateAutomaticBillPayments.class)
    private BigDecimal value;
    @NotNull(groups = ValidationGroup.CreateAutomaticBillPayments.class)
    private String currency;

    public BigDecimal getValue() {
        return value;
    }

    public void setValue(BigDecimal value) {
        this.value = value;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }
}
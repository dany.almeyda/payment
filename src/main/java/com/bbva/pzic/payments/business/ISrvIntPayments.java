package com.bbva.pzic.payments.business;

import com.bbva.pzic.payments.business.dto.DTOIntAutomaticBillPayment;
import com.bbva.pzic.payments.business.dto.DTOIntAutomaticBillPayments;
import com.bbva.pzic.payments.business.dto.InputListAutomaticBillPayments;
import com.bbva.pzic.payments.canonic.AutomaticBillPayment;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
public interface ISrvIntPayments {

    /**
     * Method for retrieving the list of automatic bill payments configured by
     * the customer.
     *
     * @param input dto with input fields to validate
     * @return {@link DTOIntAutomaticBillPayments}
     */
    DTOIntAutomaticBillPayments listAutomaticBillPayments(InputListAutomaticBillPayments input);

    /**
     * Method for performing the automatic bill payment of a service.
     *
     * @param dtoInt dto with input fields to validate
     * @return {@link AutomaticBillPayment}
     */
    AutomaticBillPayment createAutomaticBillPayments(DTOIntAutomaticBillPayment dtoInt);
}
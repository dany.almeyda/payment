package com.bbva.pzic.payments.business.dto;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
public class DTOIntServiceType {

    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
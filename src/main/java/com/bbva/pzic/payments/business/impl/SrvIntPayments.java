package com.bbva.pzic.payments.business.impl;

import com.bbva.pzic.payments.business.ISrvIntPayments;
import com.bbva.pzic.payments.business.dto.DTOIntAutomaticBillPayment;
import com.bbva.pzic.payments.business.dto.DTOIntAutomaticBillPayments;
import com.bbva.pzic.payments.business.dto.InputListAutomaticBillPayments;
import com.bbva.pzic.payments.business.dto.ValidationGroup;
import com.bbva.pzic.payments.canonic.AutomaticBillPayment;
import com.bbva.pzic.payments.dao.IPaymentsDAO;
import com.bbva.pzic.routine.validator.Validator;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created on 15/08/2018.
 *
 * @author Entelgy
 */
@Service
public class SrvIntPayments implements ISrvIntPayments {

    private static final Log LOG = LogFactory.getLog(SrvIntPayments.class);
    @Autowired
    private IPaymentsDAO paymentsDAO;
    @Autowired
    private Validator validator;

    /**
     * {@inheritDoc}
     */
    @Override
    public DTOIntAutomaticBillPayments listAutomaticBillPayments(
            final InputListAutomaticBillPayments input) {
        LOG.info("... Invoking method SrvIntPayments.listAutomaticBillPayments ...");
        return paymentsDAO.listAutomaticBillPayments(input);
    }


    @Override
    public AutomaticBillPayment createAutomaticBillPayments(
            final DTOIntAutomaticBillPayment dtoInt) {
        LOG.info("... Invoking method SrvIntPayments.createAutomaticBillPayments ...");
        LOG.info("... Validating createAutomaticBillPayments input parameter ...");
        validator.validate(dtoInt,
                ValidationGroup.CreateAutomaticBillPayments.class);
        return paymentsDAO.createAutomaticBillPayments(dtoInt);
    }
}